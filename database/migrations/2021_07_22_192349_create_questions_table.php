<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use phpDocumentor\Reflection\Types\Nullable;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('surname');
            $table->string('data_nascita');
            $table->string('email');
            $table->string('routine');
            $table->string('type_working');
            $table->string('primary_goal');
            $table->string('important_goal');
            $table->string('time_available');
            $table->string('impediment');
            $table->string('issue');
            $table->string('ready');
            $table->bigInteger('phone')->nullable();
            $table->string('other_details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
