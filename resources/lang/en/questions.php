<?php



return [
    
    'quest'=>[
        'quest'=>'questions',
    ],
    'quest1'=>[
        'quest1'=>'What is your first and last name ?',
    ],
    'quest2'=>[
        'quest2'=>'When is your birthday?',
    ],
    'quest3'=>[
        'quest3'=>'What is your email?',
    ],
    'quest4'=>[
        'quest4'=>'How many hours does your job take from your daily routinex?',
    ],
    'quest5'=>[
        'quest5'=>'Is your job physically demanding? Or are you working in an office setting?',
    ],
    'quest6'=>[
        'quest6'=>'What is your primary fitness goal?',
    ],
    'quest7'=>[
        'quest7'=>'Why is this goal important to you?',
    ],
    'quest8'=>[
        'quest8'=>'Are you ready to invest in your goals ?',
    ],
    'quest9'=>[
        'quest9'=>'What’s the biggest thing holding you back from your goal?',
    ],
    'quest10'=>[
        'quest10'=>'How long has this been an issue for you?',
    ],
    'quest11'=>[
        'quest11'=>'Our coaching program is not a one size fits all program.
    It is a hands-on 1-1 coaching experience tailored to your specific goals and needs.
    Are you ready to take FULL control and take action on your fitness goals right now?',
    ],
    'quest12'=>[
        'quest12'=>'What is your phone number ? ( So i will be able to book a call with you , we’ll talk in deep about your needs and habits ,
        and about your plan )',
    ],
    'quest13'=>[
        'quest13'=>"Is there anything else you would like to add that wasn't mentioned above?",
    ],

    ]




?>