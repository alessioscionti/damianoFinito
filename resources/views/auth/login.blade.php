<x-layoutnobg>
    <header class="jumbotron">
        <h1 class="text-center display-3" id="title"> Login </h1>
    <div class="container">
        <form id="survey-form" action="{{route('login')}}" method="POST">
            @csrf
            <h2 class="display-5"> </h2>
            <!-- TEXTB0X: LAST EMAIL -->
            <div class="form-group d-flex mt-2">
                <input class="form-control" type="email" name="email" placeholder="email">
            </div>
                    <!-- TEXTB0X: PASSWORD -->
            <div class="form-group d-flex mt-2">
                <input class="form-control" type="password" name="password" placeholder="password">
            </div>
            <!-- BUTTON: SUBMIT -->
            <button class="btn btn-primary mb-5" id="submit">
                Submit
            </button>
        </form>
        </div>
</header>
</x-layoutnobg>