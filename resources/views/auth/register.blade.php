<x-layoutnobg>


    <header class="jumbotron">
        <h1 class="text-center display-3" id="title"> Registration Form </h1>
    <div class="container">
        <form id="survey-form" action="{{route('register')}}" method="POST">
            @csrf
            <h2 class="display-5"> Basic Info </h2>
            <!-- TEXTB0X: FIRST NAME -->
            <div class="form-group d-flex mt-2">
                <input class="form-control" type="text" name="name" placeholder="Last name">
            </div>
            <!-- TEXTB0X: LAST EMAIL -->
            <div class="form-group d-flex mt-2">
                <input class="form-control" type="email" name="email" placeholder="email">
            </div>
                    <!-- TEXTB0X: PASSWORD -->
            <div class="form-group d-flex mt-2">
                <input class="form-control" type="password" name="password" placeholder="password">
            </div>
            <div class="form-group d-flex mt-2">
                <input class="form-control" type="password" name="password_confirmation" placeholder="password_confirmation">
            </div>

						<!-- GENDER -->
            <div class="form-group mt-5">
                <label for="gender">Gender</label>
                <select class="form-control" name="gender">
                    <option>Male</option>
                    <option>Female</option>
                </select>
            </div>

            <!-- BUTTON: SUBMIT -->
            <button class="btn btn-primary mb-5" id="submit">
                Submit
            </button>
        </form>
        </div>

</header>
</x-layoutnobg>