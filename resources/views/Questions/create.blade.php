<style>
  body {
    background: rgb(96, 96, 98);
    background: linear-gradient(180deg, rgba(68, 68, 68) 15%, rgba(99, 49, 49) 45%, rgb(161, 20, 20) 100%);
    background-repeat: no-repeat;
    height: 100%
}
</style>
<x-layout>
  @if (Session::has('message'))
    <div class="alert alert-success container text-center">
      {{session('message')}}
    </div>
  @endif
      
<div class="container">
  <div class="row justify-content-center mt-5">
    <div class="col-12 col-md-6">


    <form id="regForm" action="{{route('questions.store')}}" method="POST">
      @csrf
      <h1 style="color: black">{{__('questions.quest.quest')}}:</h1>
      
      <!-- One "tab" for each step in the form: -->
      <div class="col tab text-center"style="color: black">{{__('questions.quest1.quest1')}}:
        <p class="mt-3"><input placeholder="name..." class="form-control" name="name" oninput="this.className = ''"></p>
        <p class="mt-3"><input placeholder="surname..."class="form-control" name="surname" oninput="this.className = ''"></p>
      </div>
      <div class="col tab text-center"style="color: black">{{__('questions.quest2.quest2')}}
        <input type="date" name="data_nascita" oninput="this.className = ''">
      </div>
      <div class="col tab text-center"style="color: black">{{__('questions.quest3.quest3')}}
        <input type="email" name="email" oninput="this.className = ''">
      </div>
      <div class="col tab"style="color: black">{{__('questions.quest4.quest4')}}<br>
        <select class="mt-3" name="routine" id="">
          <option value="" selected disabled>select an option</option>
          <option name="sex" value="M"onselect="this.className = ''">Full time</option>
          <option name="sex" value="F"onselect="this.className = ''">Part time</option>
          <option name="sex" value="I prefer not to specify"onselect="this.className = ''">Unemployed</option>
        </select>
    </div>
      <div class="col tab"style="color: black">{{__('questions.quest5.quest5')}}
        <br>
        <textarea class="form-control"name="type_working" id="" cols="35" rows="10" style="color:black" oninput="this.className = ''"></textarea>
      </div>
      <div class="tab"style="color: black">{{__('questions.quest6.quest6')}}
        <br>
        <textarea class="form-control"name="primary_goal" id="" cols="35" rows="10" style="color:black" oninput="this.className = ''"></textarea>
      </div>
      <div class="tab"style="color: black">{{__('questions.quest7.quest7')}}
        <br>
        <textarea class="form-control"name="important_goal" id="" cols="35" rows="10" style="color:black" oninput="this.className = ''"></textarea>
      </div>
      <div class="tab"style="color: black">{{__('questions.quest8.quest8')}}
        <br>
        <select name="time_available" id="">
          <option value=""selected disabled>--</option>
          <option value="yes">Yes</option>
          <option value="no">No</option>
        </select>
      </div>
      <div class="tab"style="color: black">{{__('questions.quest9.quest9')}}
          <p>(If you have one )</p>
        <br>
        <textarea class="form-control"name="impediment" id="" cols="35" rows="10" style="color:black"></textarea>
      </div>
      <div class="tab"style="color: black">{{__('questions.quest10.quest10')}}
        <br>
        <textarea class="form-control"name="issue" id="" cols="35" rows="10" style="color:black"></textarea>
      </div>
      <div class="tab"style="color: black;font-size:0.9rem;">{{__('questions.quest11.quest11')}}
        <br>
        <select name="ready" id="">
          <option value=""selected disabled>--</option>
          <option value="yes">Yes</option>
          <option value="no">No</option>
        </select>
      </div>
      <div class="tab"style="color: black">{{__('questions.question.quest12.quest12')}}        
        <br>
        <input type="number" name="phone">
      </div>
      <div class="tab"style="color: black">{{__('questions.question.quest13.quest13')}}
        <br>
        <textarea class="form-control"name="other_details" id="" cols="35" rows="10" style="color:black"></textarea>
      </div>
      <div class="d-flex justify-content-center mt-4" style="overflow:auto;">
        <div style="float:right;">
          <button class="btn btn-dark" type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
          <button class="btn btn-dark" type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
        </div>
      </div>

      <!-- Circles which indicates the steps of the form: -->
      <div style="text-align:center;margin-top:60px;">
        <span class="step"></span>
        <span class="step"></span>
        <span class="step"></span>
        <span class="step"></span>
        <span class="step"></span>
        <span class="step"></span>
        <span class="step"></span>
        <span class="step"></span>
        <span class="step"></span>
        <span class="step"></span>
        <span class="step"></span>
        <span class="step"></span>
        <span class="step"></span>
      </div>
      </form>
    </div>
  </div>
</div>
<script>

        var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}

</script>

</x-layout>