<style>
  
  .border-bottom{
    border-bottom-color: white!important;
    border-radius: 0%!important;
    
  }
</style>
<div class="container">
    <div class="row justify-content-center mt-5 drop drop-1 border-bottom">
      <div class="col-12">
            <nav class="navbar d-flex justify-content-center navbar-expand-sm navbar-light">
              <div class="col-1">
                  <a href="{{route('homepage')}}"><img src="/img/bodu2.png" alt="" style="width:350px;margin-left:-1.5rem;margin-right:-10rem;"></a>
                  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
                </div>
                <div class="col-10">
                  <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0 me-auto">
                      <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="{{route('homepage')}}" style="color:white;font-size:1rem;">HOME</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#" style="color:white;font-size:1rem;">CONTATTI</a>
                      </li>
                      @guest
                      <li class="nav-item">
                        <a class="nav-link" href="{{route('login')}}" style="color:white;font-size:1rem;">LOGIN</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{route('register')}}" style="color:white;font-size:1rem;">REGISTER</a>
                      </li>
                      @endguest
                      <li class="nav-item">
                        <a class="nav-link" href="{{route('questions.create')}}" style="color:white;font-size:1rem;">{{__('questions.quest.quest')}}</a>
                      </li>

                      <li>
                        <form action="{{route('langChange','en')}}" method="POST">
                          @csrf
                          <button type="submit" class="nav-link" style="border:none;background:none">
                            <img src="/img/usa_new.png" alt="" width="25" height="20"></span>
                          </button>
                        </form>
                      </li>
                      <li>
                        <form action="{{route('langChange','it')}}" method="POST">
                          @csrf
                          <button class="nav-link" style="border:none;background:none">
                           <img src="/img/Italy_new.png" alt="" width="25" height="20"></button>
                        </form>
                      </li>

                    </ul>
                  </div>
                </div>
            </nav>
            
      </div>
    </div>
</div>

<!--NAVBAR TEST-->
{{-- <div class="wrap">
  <h1>Glassmorphism</h1>
  <div class="drop drop1">
     <div class="drop drop2"></div>
     <div class="drop drop3"></div>
     <div class="drop drop4"></div>
  </div>
</div> --}}
<!--FINE NAVBAR TEST-->