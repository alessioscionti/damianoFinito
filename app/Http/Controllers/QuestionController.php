<?php

namespace App\Http\Controllers;

use App\Models\Question;
use GuzzleHttp\Psr7\Message;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /* dd($request->all()); */
        
        $question = Question::create([
            'name' => $request->name,
            'surname' => $request->surname,
            'data_nascita' => $request->data_nascita,
            'email' => $request->email,
            'routine' => $request->routine,
            'type_working' => $request->type_working,
            'primary_goal' => $request->primary_goal,
            'important_goal' => $request->important_goal,
            'time_available' => $request->time_available,
            'impediment' => $request->impediment,
            'issue' => $request->issue,
            'ready' => $request->ready,
            'phone' => $request->phone,
            'other_details' => $request->other_details,
            
        ]);
            
        return back()->with('message', 'form compilato correttamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        //
    }
}
