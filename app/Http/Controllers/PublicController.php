<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function Homepage(){
        return view('homepage');
    }

    public function Question(){
        return view('questions');
    }

    public function locale($locale){
        /* dd($locale); */
        session()->put('lang', $locale);
        return redirect()->back();
    }
}
