<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $fillable = [
         'name',
         'surname',
         'data_nascita',
         'email',
         'routine',
         'type_working',
         'primary_goal',
         'important_goal',
         'time_available',
         'impediment',
         'issue',
         'ready',
         'phone',
         'other_details',
         
    ];
}
